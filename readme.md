# Boot Firmware for Genio Ubuntu

This repository stores prebuilt binary of bootloaders and other firmware binaries intended
to boot into [Ubuntu on Genio EVKs](https://mediatek.gitlab.io/genio/doc/ubuntu).

Please note that these bootloader binaries are only compatible with Genio EVK boards,
such as Genio 1200 EVK and Genio 700 EVK. They don't work on other boards, even if the
board is using Genio SoC.

These firmware files are designed for development and evaluation purposes.
These firmware files are distributed in the hope that they will be useful, but
are provided WITHOUT ANY WARRANTY, INCLUDING BUT NOT LIMITED TO IMPLIED WARRANTY
OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.

## Build Information

These firmware are built using [IoT Yocto](https://mediatek.gitlab.io/aiot/doc/aiot-dev-guide).
For build instructions, please visit
[Genio Ubuntu documentation](https://mediatek.gitlab.io/genio/doc/ubuntu/customization/customize-bootloader.html).

## Branches

The purpose of each branch:

- `upstream/iot-yocto`: This branch stores prebuilt binary board assets built from IoT Yocto. This
  branch should always follow the prebuilt output from IoT Yocto.

- `main`: Based on IoT Yocto board assets, modify the u-boot-initial-env to switch
  to EBBR boot on eMMC or UFS storage, and additional boot environment modifications for Ubuntu.

## Maintainer Notes

A typical workflow is to build new board assets with IoT Yocto, commit the changes into
`upstream/iot-yocto`, and then merge the new updates to `main`, and then add additional fixes
and configurations required by Ubuntu in the `main` branch.

Steps:

1. Build or retrieve the built `board-assets` binaries from IoT Yocto project for each board.
   Note that the sub-directory names, e.g. `genio-1200-evk-ufs`, map to IoT Yocto `MACHINE` names.

2. Import the unmodified IoT Yocto `board-assets`, along with `u-boot-initial-env` in to `upstream/iot-yocto` branch

3. Update `snapshot.xml` to match the exact commit hashes of the IoT Yocto project sources, so users of these binaries
   can rebuild these bootloaders from IoT Yocto project from scratch.

4. Merge the updates from `upstream/iot-yocto` to `main` branch. Make sure the dtb/dtbo/u-boot-initial-env match the requirement
   for Genio Ubuntu.

## Changelog & Version Format

The file `changelog` provides the histories of each release. The format loosely follows a typical Debian package changelog.
The timestamp in the changelog is in this format: `DDD, DD MMM YYYY hh:mm:ss ZZZZ`.

The package version string format is:

```
<upstream iot yocto version>.<package version>
```

- `<iot yocto version>` can be `23.1` or `23.1.1`, following the release versions of IoT Yocto.
- `<package version>` is optional, it is used to describe package development releases:

    * Empty package version denotes stable releases.
    * `pr<N>` prefix denotes Preview releases, e.g. `pr1`, `pr2`, and so on.
    * `dev<YYMMDD>` suffixes are development releases based on daily builds.

## Files and Directories

Each directory provides board assets for a specific board.
The board name should match the IoT Yocto `MACHINE` name for Genio EVK, if possible.

Inside each board assets directory, there should be these files:

* `bl2.img` is the BL2 bootloader
* `bootassets.vfat` provides resources for U-Boot, such as boot logo.
* `fip.bin` is a FIP package containing BL31(Trusted-Firmware-A), BL32(OP-TEE), and BL33(U-Boot).
* `lk.bin` is used by `genio-tools` to bootstrap the board to write eMMC/UFS storages through `USB0` port.
* `snapshot.xml` provides a repo manifest file that captures the IoT Yocto recipes used to build the board assets.
* `u-boot-initial-env` provides the boot script and environment variables for U-Boot.

Note: The `firmware.vfat` partition file, which stores device tree files for Genio EVK,
is expected to be part of the Genio Ubuntu image archive. It is not part of the boot firmware.
