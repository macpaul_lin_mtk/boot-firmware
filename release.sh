#!/bin/sh

# Create "build" directory
mkdir -p build

# Generate tarballs for each "genio*" directory
# Example archive name: ubuntu-boot-firmware-genio-350-evk-v23.1.dev202307271530-1-g11f8055.tar.gz
\ls -1 | grep genio | xargs -I {} git archive -v --prefix "boot-firmware-" --format tar.gz -o ./build/ubuntu-boot-firmware-{}-v`git describe --tag`.tar.gz HEAD {}
